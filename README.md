# Docker images of mine
On occasion I have needed more than was available on vanilla Docker images and
kept losing my work. Storing all the dockerfiles here now.

## Jenkins
To build it:

```
BUILD_NUMBER=`date +"%s"`
docker build -t mvmjenkins:${BUILD_NUMBER} .
docker tag -f mvmjenkins:${BUILD_NUMBER} mvmjenkins:latest
```

To run it:

## Postgres 9.5 with PostGis
This is the stock PostgreSQL docker image with PostGis tagged on an en_GB language set.

Should have a __apt-get update -y__ but that kept failing. Investigate!

To build it:

```
BUILD_NUMBER=`date +"%s"`
docker build -t mvmpostgres:${BUILD_NUMBER} .
docker tag -f mvmpostgres:${BUILD_NUMBER} mvmpostgres:latest
```

## Spring Cloud Config Server
Built for a Tutum demo, needs more work.

To build it:

```
BUILD_NUMBER=`date +"%s"`
docker build -t mvmspringconfig:${BUILD_NUMBER} .
docker tag -f mvmspringconfig:${BUILD_NUMBER} mvmspringconfig:latest
```
